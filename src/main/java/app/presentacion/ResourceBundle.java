package app.presentacion;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceBundle {

	static final Logger LOG = LoggerFactory.getLogger(ResourceBundle.class);

	private String nombre, alias, dir;
	private ArrayList<String> pages;
	private ResourcePackage resourcePackage;
	private boolean inSidebar;

	public ResourceBundle(ResourcePackage paquete) {
		this.pages = new ArrayList<>();
		this.resourcePackage = paquete;
	}

	public String getPathname() {
		return resourcePackage.getPathname() + dir;
	}

	public String getPagePathname() {
		return resourcePackage.getPathname() + dir + getZul();
	}

	public String getPagePathname(String sub) {
		return resourcePackage.getPathname() + dir + getZul(sub);
	}

	@Override
	public int hashCode() {
		return alias.toLowerCase().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof ResourceBundle) && ((ResourceBundle) o).getAlias().toLowerCase().equals(getNombre());
	}

	public String getNombre() {
		return nombre;
	}

	public String getAlias() {
		return alias;
	}

	public String getDir() {
		return dir;
	}

	public void addZul(String zul) {
		pages.add(zul);
	}

	public String getZul() {
		return pages.get(0);
	}

	public String getZul(String sub) {
		String zul = null;
		int c = 0;
		if (sub != null)
			for (String z : pages)
				if (z.toLowerCase().contains(sub.toLowerCase())) {
					zul = z;
					c = c + 1;
				}
		return c == 1 ? zul : null;
	}

	public ResourcePackage getPaquete() {
		return resourcePackage;
	}

	public boolean isEnSidebar() {
		return inSidebar;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public void setResourcePackage(ResourcePackage paquete) {
		this.resourcePackage = paquete;
	}

	public void setInSidebar(boolean enSidebar) {
		this.inSidebar = enSidebar;
	}

}
