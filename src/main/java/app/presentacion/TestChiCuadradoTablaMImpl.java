package app.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zul.ListModelList;

import app.DetalleTabla;
import app.Tabla;

@Component
public class TestChiCuadradoTablaMImpl implements TestChiCuadradoTablaM {

	static final Logger LOG = LoggerFactory.getLogger(TestChiCuadradoTablaMImpl.class);
	Tabla tabla;
	@Autowired
	ApplicationContext context;
	
	public TestChiCuadradoTablaMImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TestChiCuadradoTablaM testChiCuadradoTablaM() {
		return new TestChiCuadradoTablaMImpl();
	}

	@Override
	public void setTabla(Tabla tabla) {
		this.tabla = tabla;
	}

	@Override
	public Object getEncabezadoA() {
		return tabla.getEncabezados().get(0);
	}

	@Override
	public Object getEncabezadoB() {
		return tabla.getEncabezados().get(1);
	}

	@Override
	public Object getEncabezadoC() {
		return tabla.getEncabezados().get(2);
	}

	@Override
	public Object getEncabezadoD() {
		return tabla.getEncabezados().get(3);
	}

	@Override
	public Object getEncabezadoE() {
		return tabla.getEncabezados().get(4);
	}

	@Override
	public Object getEncabezadoF() {
		return tabla.getEncabezados().get(5);
	}

	@Override
	public Object getEncabezadoG() {
		return tabla.getEncabezados().get(6);
	}

	@Override
	public Object getEncabezadoH() {
		return tabla.getEncabezados().get(7);
	}
	
	@Override
	public ListModelList<TestChiCuadradoDetalleM> getDetalles() {
		ListModelList<TestChiCuadradoDetalleM> modelo = new ListModelList<>();
		for (DetalleTabla d : tabla.getDetalles()) {
			TestChiCuadradoDetalleMImpl dM = (TestChiCuadradoDetalleMImpl) context.getBean("testChiCuadradoDetalleM");
			dM.setDetalleTabla(d);
			modelo.add(dM);
		}
		return modelo;
	}

}
