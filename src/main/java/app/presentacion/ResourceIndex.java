package app.presentacion;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

@Service("resourceIndex")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ResourceIndex {

	static final Logger LOG = LoggerFactory.getLogger(ResourceIndex.class);

	private HashMap<String, ResourceBundle> cU;
	private HashMap<String, ResourcePackage> paqMap;
	private ArrayList<ResourcePackage> paqList;

	ResourceIndex() { }

	@PostConstruct
	public void init() {

		cU = new HashMap<String, ResourceBundle>(100, 0.5f);
		paqMap = new HashMap<String, ResourcePackage>(10, 0.5f);
		paqList = new ArrayList<ResourcePackage>();
		ResourceBundle c;
		ResourcePackage paq;

		// Inicio de la definición del paquete
		// "Trabajo Práctico 1"
		paq = new ResourcePackage(null);
		paq.setAlias("gagafg");
		paq.setDir("./pages/content/tp1/");
		paq.setNombre("Trabajo Práctico 1");
		paqMap.put(paq.getAlias().toLowerCase(), paq);
		paqList.add(paq);

		c = new ResourceBundle(paq);
		c.setAlias("kjebvm");
		c.setDir("puntoa/");
		c.setNombre("Punto a)");
		c.addZul("tp1-puntoa.zul");
		c.addZul("tp1-puntoa-enunciado.zul");
		c.addZul("tp1-puntoa-solucion.zul");
		c.addZul("tp1-puntoa-input-lineal.zul");
		c.addZul("tp1-puntoa-input-multiplicativo.zul");
		c.addZul("tp1-puntoa-resultados.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);

		c = new ResourceBundle(paq);
		c.setAlias("lypmvw");
		c.setDir("puntob/");
		c.setNombre("Punto b)");
		c.addZul("tp1-puntob.zul");
		c.addZul("tp1-puntob-enunciado.zul");
		c.addZul("tp1-puntob-solucion.zul");
		c.addZul("tp1-puntob-input.zul");
		c.addZul("tp1-puntob-resultados.zul");
		c.addZul("tp1-puntob-grafica.zul");
		c.addZul("tp1-puntob-chicuadrado.zul");
		c.addZul("tp1-puntob-chicuadrado-tabla.zul");
		c.addZul("tp1-puntob-tablafrecuencias.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);

		c = new ResourceBundle(paq);
		c.setAlias("hhutad");
		c.setDir("puntoc/");
		c.setNombre("Punto c)");
		c.addZul("tp1-puntoc.zul");
		c.addZul("tp1-puntoc-enunciado.zul");
		c.addZul("tp1-puntoc-solucion.zul");
		c.addZul("tp1-puntoc-input.zul");
		c.addZul("tp1-puntoc-resultados.zul");
		c.addZul("tp1-puntoc-grafica.zul");
		c.addZul("tp1-puntoc-chicuadrado.zul");
		c.addZul("tp1-puntoc-chicuadrado-tabla.zul");
		c.addZul("tp1-puntoc-tablafrecuencias.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);
		// Fin de la definición del paquete "Trabajo Práctico 1"

	}

	public ResourcePackage getResourcePackage(String alias) {
		ResourcePackage paquete = null;
		paquete = paqMap.get(alias.toLowerCase());
		return paquete;
	}

	public ResourceBundle getResourceBundle(String alias) {
		ResourceBundle casoUso = null;
		casoUso = cU.get(alias.toLowerCase());
		return casoUso;
	}

	public ArrayList<ResourcePackage> getResourcePackages() {
		return paqList;
	}

}
