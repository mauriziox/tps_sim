package app.presentacion;

import org.zkoss.zul.ListModelList;

import app.Tabla;

public interface TestChiCuadradoTablaM {

	void setTabla(Tabla tabla);
	
	Object getEncabezadoA();

	Object getEncabezadoB();

	Object getEncabezadoC();

	Object getEncabezadoD();

	Object getEncabezadoE();

	Object getEncabezadoF();

	Object getEncabezadoG();

	Object getEncabezadoH();

	ListModelList<TestChiCuadradoDetalleM> getDetalles();

}
