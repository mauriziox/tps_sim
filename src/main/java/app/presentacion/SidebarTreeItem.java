package app.presentacion;

public class SidebarTreeItem {
	
	private String name;
	private String uri;
	
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public SidebarTreeItem(String name, String uri) {
		super();
		this.name = name;
		this.uri = uri;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
