package app.presentacion;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.encoders.EncoderUtil;
import org.jfree.chart.encoders.ImageFormat;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.select.annotation.VariableResolver;

import app.utils.Histograma;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class HistogramaVM {

	static final Logger LOG = LoggerFactory.getLogger(HistogramaVM.class);
	Histograma histograma;
	AImage imagen;
	String serieAMostrar;

	@Init
	public void initSetup(@BindingParam("histograma") Histograma histograma) {
		this.histograma = histograma;
		setSerieAMostrar("Frecuencias observadas");
		imagen = generarImagen(generarGrafica(crearDataset(histograma), histograma));
	}

	@Command
	@NotifyChange("imagen")
	public void cambiarImagen() {
		imagen = generarImagen(generarGrafica(crearDataset(histograma), histograma));
	}

	private IntervalXYDataset crearDataset(Histograma histograma) {
		HistogramDataset dataSet = new HistogramDataset();
		if (serieAMostrar.equals("Frecuencias observadas"))
			dataSet.addSeries("Frecuencias observadas", histograma.getSerie(), histograma.getIntervalos().length, 0.0d,
					1.0d);
		if (serieAMostrar.equals("Frecuencias esperadas"))
			dataSet.addSeries("Frecuencias esperadas", histograma.getSerieUniforme(), histograma.getIntervalos().length,
					0.0d, 1.0d);
		if (serieAMostrar.equals("Ambas frecuencias")) {
			dataSet.addSeries("Frecuencias observadas", histograma.getSerie(), histograma.getIntervalos().length, 0.0d,
					1.0d);
			dataSet.addSeries("Frecuencias esperadas", histograma.getSerieUniforme(), histograma.getIntervalos().length,
					0.0d, 1.0d);
		}
		return (IntervalXYDataset) dataSet;
	}

	private JFreeChart generarGrafica(IntervalXYDataset dataset, Histograma histograma) {
		JFreeChart grafica = ChartFactory.createHistogram("", "Intervalo", "Frecuencia",
				dataset, PlotOrientation.VERTICAL, true, true, false);
		XYPlot xYPlot = (XYPlot) grafica.getPlot();
		xYPlot.setDomainPannable(true);
		xYPlot.setRangePannable(true);
		xYPlot.setForegroundAlpha(0.85F);
		NumberAxis numberAxis = (NumberAxis) xYPlot.getRangeAxis();
		numberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYBarRenderer xYBarRenderer = (XYBarRenderer) xYPlot.getRenderer();
		xYBarRenderer.setDrawBarOutline(true);
		xYBarRenderer.setBarPainter(new StandardXYBarPainter());
		xYBarRenderer.setShadowVisible(false);
		xYPlot.getRangeAxis().setUpperMargin(xYPlot.getRangeAxis().getUpperMargin() + 0.1);
		NumberAxis dominio = (NumberAxis) xYPlot.getDomainAxis();
		TickUnits tickUnits = new TickUnits();
		tickUnits.add(new NumberTickUnit(histograma.getLongitud()));
		tickUnits.add(new NumberTickUnit(histograma.getLongitud() * 2.0d));
		tickUnits.add(new NumberTickUnit(histograma.getLongitud() * 3.0d));
		tickUnits.add(new NumberTickUnit(histograma.getLongitud() * 4.0d));
		tickUnits.add(new NumberTickUnit(histograma.getLongitud() * 5.0d));
		tickUnits.add(new NumberTickUnit(histograma.getLongitud() * 6.0d));
		tickUnits.add(new NumberTickUnit(histograma.getLongitud() * 7.0d));
		tickUnits.add(new NumberTickUnit(histograma.getLongitud() * 8.0d));
		tickUnits.add(new NumberTickUnit(histograma.getLongitud() * 9.0d));
		dominio.setStandardTickUnits(tickUnits);
		return grafica;
	}

	private AImage generarImagen(JFreeChart grafica) {
		BufferedImage bi = grafica.createBufferedImage(500, 300, BufferedImage.TRANSLUCENT, null);
		byte[] bytes = null;
		AImage image = null;
		try {
			bytes = EncoderUtil.encode(bi, ImageFormat.PNG, true);
			image = new AImage(null, bytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return image;
	}

	public AImage getImagen() {
		return imagen;
	}

	public void setImagen(AImage imagen) {
		this.imagen = imagen;
	}

	public String getSerieAMostrar() {
		return serieAMostrar;
	}

	public void setSerieAMostrar(String serieAMostrar) {
		this.serieAMostrar = serieAMostrar;
	}

}
