package app.presentacion;

public interface Observer {

	void update(Observable o, Object arg);

}
