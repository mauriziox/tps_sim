package app.presentacion;

public interface Observable {

	void addObserver(Observer o);

	int countObservers();

	void deleteObserver(Observer o);

	void deleteObservers();

	boolean	hasChanged();

	void notifyObservers();

	void notifyObservers(Object arg);

}
