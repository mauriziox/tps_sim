package app.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.utils.Histograma;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaFrecuenciasVM {

	static final Logger LOG = LoggerFactory.getLogger(TablaFrecuenciasVM.class);
	@WireVariable
	TablaFrecuenciasM tablaFrecuenciasM;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;


	@Init
	public void initSetup(@BindingParam("histograma") Histograma histograma) {
		LOG.debug("initSetup() histograma: " + histograma);
		LOG.debug("initSetup() tablaFrecuenciasM: " + tablaFrecuenciasM);
		tablaFrecuenciasM.setTabla(histograma.getTabla());
	}
	
	public ListModelList<TablaFrecuenciasDetalleM> getDetalles() {
		return tablaFrecuenciasM.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public TablaFrecuenciasM getTablaFrecuenciasM() {
		return tablaFrecuenciasM;
	}

}
