package app.presentacion;

import org.zkoss.zul.ListModelList;

import app.Tabla;

public interface TablaFrecuenciasM {

	void setTabla(Tabla tabla);
	
	Object getEncabezadoA();

	Object getEncabezadoB();

	Object getEncabezadoC();

	Object getEncabezadoD();

	ListModelList<TablaFrecuenciasDetalleM> getDetalles();

}
