package app.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zul.ListModelList;

import app.DetalleTabla;
import app.Tabla;

@Component
public class TablaFrecuenciasMImpl implements TablaFrecuenciasM {

	static final Logger LOG = LoggerFactory.getLogger(TablaFrecuenciasMImpl.class);
	Tabla tabla;
	@Autowired
	ApplicationContext context;
	
	public TablaFrecuenciasMImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TablaFrecuenciasM  tablaFrecuenciasM() {
		return new TablaFrecuenciasMImpl();
	}

	@Override
	public void setTabla(Tabla tabla) {
		this.tabla = tabla;
	}

	@Override
	public Object getEncabezadoA() {
		return tabla.getEncabezados().get(0);
	}

	@Override
	public Object getEncabezadoB() {
		return tabla.getEncabezados().get(1);
	}

	@Override
	public Object getEncabezadoC() {
		return tabla.getEncabezados().get(2);
	}

	@Override
	public Object getEncabezadoD() {
		return tabla.getEncabezados().get(3);
	}

	@Override
	public ListModelList<TablaFrecuenciasDetalleM> getDetalles() {
		ListModelList<TablaFrecuenciasDetalleM> modelo = new ListModelList<>();
		for (DetalleTabla d : tabla.getDetalles()) {
			TablaFrecuenciasDetalleMImpl dM = (TablaFrecuenciasDetalleMImpl) context.getBean("tablaFrecuenciasDetalleM");
			dM.setDetalleTabla(d);
			modelo.add(dM);
		}
		return modelo;
	}

}
