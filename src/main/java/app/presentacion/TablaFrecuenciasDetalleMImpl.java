package app.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.DetalleTabla;

@Component
public class TablaFrecuenciasDetalleMImpl implements TablaFrecuenciasDetalleM {
	
	static final Logger LOG = LoggerFactory.getLogger(TablaFrecuenciasDetalleMImpl.class);
	DetalleTabla detalleTabla;
	
	public TablaFrecuenciasDetalleMImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TablaFrecuenciasDetalleMImpl  tablaFrecuenciasDetalleM () {
		return new TablaFrecuenciasDetalleMImpl ();
	}

	void setDetalleTabla(DetalleTabla detalleTabla) {
		this.detalleTabla = detalleTabla;
	}
	
	@Override
	public Object getValorA() {
		return detalleTabla.getValores().get("k");
	}

	@Override
	public Object getValorB() {
		return detalleTabla.getValores().get("Intervalo");
	}

	@Override
	public Object getValorC() {
		return detalleTabla.getValores().get("fo");
	}

	@Override
	public Object getValorD() {
		return detalleTabla.getValores().get("fe");
	}

}
