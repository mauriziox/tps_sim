package app.presentacion;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.BindContext;

@Component
public class ConversorDeIntImpl implements ConversorDeInt {

	public ConversorDeIntImpl() {
		super();
	}

	@Bean
	@Scope("singleton")
	public ConversorDeInt conversorDeInt() {
		return new ConversorDeIntImpl();
	}

	@Override
	public Object coerceToBean(Object arg0, org.zkoss.zk.ui.Component arg1, BindContext arg2) {
		Object o = null;
		try {
			if (arg0 != null && !((String) arg0).isEmpty())
				o = Integer.parseInt((String) arg0);
		} catch (NumberFormatException ex) {

		}
		return o;
	}

	@Override
	public Object coerceToUi(Object val, org.zkoss.zk.ui.Component comp, BindContext ctx) {
		Object o = null;
		if (val != null)
			o = String.format("%d", val);
		return o;
	}

}
