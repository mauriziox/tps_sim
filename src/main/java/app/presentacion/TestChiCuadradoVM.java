package app.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.utils.Histograma;
import app.utils.TestChiCuadrado;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TestChiCuadradoVM {

	static final Logger LOG = LoggerFactory.getLogger(TestChiCuadradoVM.class);
	String etiqueta;
	double chiCuadrado;
	@WireVariable
	TestChiCuadrado testChiCuadrado;
	@WireVariable
	TestChiCuadradoTablaM testChiCuadradoTablaM;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;


	@Init
	public void initSetup(@BindingParam("histograma") Histograma histograma) {
		etiqueta = "\u03C7\u2072: ";
		chiCuadrado = testChiCuadrado.calcular(histograma);
		testChiCuadradoTablaM.setTabla(testChiCuadrado.getTabla());
	}
	
	public String getEtiqueta() {
		return etiqueta;
	}

	public double getChiCuadrado() {
		return chiCuadrado;
	}
	
	public ListModelList<TestChiCuadradoDetalleM> getDetalles() {
		return testChiCuadradoTablaM.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public TestChiCuadradoTablaM getTestChiCuadradoTablaM() {
		return testChiCuadradoTablaM;
	}

}
