package app.tp1.puntoa;

import java.util.List;

import app.utils.GeneradorCongruencialLineal;
import app.utils.GeneradorCongruencialMultiplicativo;

public interface TP1PuntoA {

	void generarLista(GeneradorCongruencialLineal generador);

	void generarLista(GeneradorCongruencialMultiplicativo generador);

	List<Double> getLista();
	
	void next();

	void reiniciar();

}
