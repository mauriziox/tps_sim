package app.tp1.puntoa;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.GeneradorCongruencialLineal;
import app.utils.GeneradorCongruencialMultiplicativo;
import app.utils.GeneradorPseudoaleatorio;
import app.utils.GeneradorUniformeTransformadaInversaAB;

@Component
public class TP1PuntoAImpl implements TP1PuntoA {

	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoAImpl.class);
	List<Double> lista;
	GeneradorPseudoaleatorio generador;
	@Autowired
	GeneradorUniformeTransformadaInversaAB generadorUniformeTransformadaInversaAB;

	public TP1PuntoAImpl() {
		super();
		lista = new ArrayList<>();
	}

	@Bean
	@Scope("prototype")
	static public TP1PuntoA puntoA() {
		return new TP1PuntoAImpl();
	}

	@Override
	public void generarLista(GeneradorCongruencialLineal generador) {
		reiniciar();
		generadorUniformeTransformadaInversaAB.setA(0d);
		generadorUniformeTransformadaInversaAB.setB(9999d);
		for (int i = 0; i < 20; i++)
			lista.add(generadorUniformeTransformadaInversaAB.next(generador.next()));
		this.generador = generador;
	}

	@Override
	public void generarLista(GeneradorCongruencialMultiplicativo generador) {
		reiniciar();
		generadorUniformeTransformadaInversaAB.setA(0d);
		generadorUniformeTransformadaInversaAB.setB(9999d);
		for (int i = 0; i < 20; i++)
			lista.add(generadorUniformeTransformadaInversaAB.next(generador.next()));
		this.generador = generador;
	}

	@Override
	public void reiniciar() {
		lista.clear();
		if (generador != null)
			generador.reiniciar();
	}

	@Override
	public void next() {
		lista.add(generadorUniformeTransformadaInversaAB.next(generador.next()));
	}

	public List<Double> getLista() {
		return lista;
	}

}
