package app.tp1.puntoc.presentacion;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.presentacion.FormateadorDeNumerosDecimales;
import app.presentacion.IngresarCantidadValidator;
import app.presentacion.IngresarConstanteAditivaValidator;
import app.presentacion.IngresarConstanteMultiplicativaValidator;
import app.presentacion.IngresarModuloValidator;
import app.presentacion.IngresarSemillaValidator;
import app.presentacion.IngresarSubintervalosValidator;
import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.presentacion.ResourceBundle;
import app.presentacion.ResourceIndex;
import app.presentacion.SidebarTreeItem;
import app.tp1.puntoc.TP1PuntoC;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TP1PuntoCSolucionVM implements Observer {

	private static final String TILDE_SRC = "./images/check_mark.png";
	private static final String ADVERTENCIA_SRC = "./images/warning.png";
	private static final String ASTERISCO_SRC = "./images/asterisk_red.png";
	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoCSolucionVM.class);
	@WireVariable
	private ResourceIndex resourceIndex;
	@WireVariable
	private InputValidator ingresarCantidadValidator;
	@WireVariable
	private InputValidator ingresarSubintervalosValidator;
	@WireVariable
	private InputValidator ingresarSemillaValidator;
	@WireVariable
	private InputValidator ingresarConstanteAditivaValidator;
	@WireVariable
	private InputValidator ingresarConstanteMultiplicativaValidator;
	@WireVariable
	private InputValidator ingresarModuloValidator;
	private ResourceBundle rB;
	private String ingresarCantidadIconSrc, ingresarSubintervalosIconSrc, ingresarSemillaIconSrc,
			ingresarConstanteMultiplicativaIconSrc, ingresarConstanteAditivaIconSrc, ingresarModuloIconSrc;
	private boolean cantidadValidada, subintervalosValidada, semillaValidada, constanteMultiplicativaValidada,
			constanteAditivaValidada, moduloValidada;
	@WireVariable
	private TP1PuntoC puntoC;
	ListModelList<Double> lista;
	@WireVariable
	FormateadorDeNumerosDecimales formateadorDeNumerosDecimales;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeLong conversorDeLong;
	private boolean mostrandoSolucion;

	@Init
	public void initSetup() {
		rB = resourceIndex.getResourceBundle("hhutad");
		lista = new ListModelList<>();
		ingresarCantidadValidator.addObserver(this);
		ingresarSubintervalosValidator.addObserver(this);
		ingresarSemillaValidator.addObserver(this);
		ingresarConstanteMultiplicativaValidator.addObserver(this);
		ingresarConstanteAditivaValidator.addObserver(this);
		ingresarModuloValidator.addObserver(this);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	private void inicializarValidaciones() {
	}

	private void inicializarIconosValidaciones() {
		setIngresarCantidadIconSrc(ASTERISCO_SRC);
		setIngresarSubintervalosIconSrc(ASTERISCO_SRC);
		setIngresarSemillaIconSrc(ASTERISCO_SRC);
		setIngresarConstanteMultiplicativaIconSrc(ASTERISCO_SRC);
		setIngresarConstanteAditivaIconSrc(ASTERISCO_SRC);
		setIngresarModuloIconSrc(ASTERISCO_SRC);
	}

	@GlobalCommand
	@NotifyChange({ "lista", "semillaValidada", "constanteMultiplicativaValidada", "constanteAditivaValidada",
			"ingresarSemillaIconSrc", "ingresarConstanteMultiplicativaIconSrc", "ingresarConstanteAditivaIconSrc",
			"cantidadValidada", "subintervalosValidada", "ingresarCantidadIconSrc", "ingresarSubintervalosIconSrc",
			"moduloValidada", "ingresarModuloIconSrc" })
	public void showContent(@BindingParam("page") SidebarTreeItem page) {
		if (page == null || page.getUri() == null || !page.getUri().equals(rB.getZul()))
			clear();
	}

	@Command
	@NotifyChange({ "lista", "moduloValidada", "semillaValidada", "constanteMultiplicativaValidada",
			"constanteAditivaValidada", "ingresarModuloIconSrc", "ingresarSemillaIconSrc",
			"ingresarConstanteMultiplicativaIconSrc", "ingresarConstanteAditivaIconSrc", "cantidadValidada",
			"subintervalosValidada", "ingresarCantidadIconSrc", "ingresarSubintervalosIconSrc", "mostrandoSolucion" })
	public void clear() {
		puntoC.reiniciar();
		setCantidadValidada(false);
		setSubintervalosValidada(false);
		setSemillaValidada(false);
		setConstanteMultiplicativaValidada(false);
		setConstanteAditivaValidada(false);
		setModuloValidada(false);
		setMostrandoSolucion(false);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	@Command
	@NotifyChange({ "puntoC", "graficaSrc", "chiCuadradoSrc", "tablaFrecuenciasSrc", "mostrandoSolucion" })
	public void run() {
		puntoC.solucionar();
		BindUtils.postNotifyChange(null, null, puntoC, "histograma");
		setMostrandoSolucion(true);
	}

	// "Validación"
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof IngresarCantidadValidator)
			updateValidacion((IngresarCantidadValidator) o);
		if (o instanceof IngresarSubintervalosValidator)
			updateValidacion((IngresarSubintervalosValidator) o);
		if (o instanceof IngresarSemillaValidator)
			updateValidacion((IngresarSemillaValidator) o);
		if (o instanceof IngresarConstanteMultiplicativaValidator)
			updateValidacion((IngresarConstanteMultiplicativaValidator) o);
		if (o instanceof IngresarConstanteAditivaValidator)
			updateValidacion((IngresarConstanteAditivaValidator) o);
		if (o instanceof IngresarModuloValidator)
			updateValidacion((IngresarModuloValidator) o);
	}

	private void updateValidacion(IngresarCantidadValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarCantidadIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setCantidadValidada(true);
		else
			setCantidadValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarCantidadIconSrc");
		BindUtils.postNotifyChange(null, null, this, "cantidadValidada");
	}

	private void updateValidacion(IngresarSubintervalosValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarSubintervalosIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setSubintervalosValidada(true);
		else
			setSubintervalosValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarSubintervalosIconSrc");
		BindUtils.postNotifyChange(null, null, this, "subintervalosValidada");
	}

	private void updateValidacion(IngresarSemillaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarSemillaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setSemillaValidada(true);
		else
			setSemillaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarSemillaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "semillaValidada");
	}

	private void updateValidacion(IngresarConstanteMultiplicativaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarConstanteMultiplicativaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setConstanteMultiplicativaValidada(true);
		else
			setConstanteMultiplicativaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarConstanteMultiplicativaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "constanteMultiplicativaValidada");
	}

	private void updateValidacion(IngresarConstanteAditivaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarConstanteAditivaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setConstanteAditivaValidada(true);
		else
			setConstanteAditivaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarConstanteAditivaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "constanteAditivaValidada");
	}

	private void updateValidacion(IngresarModuloValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarModuloIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setModuloValidada(true);
		else
			setModuloValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarModuloIconSrc");
		BindUtils.postNotifyChange(null, null, this, "moduloValidada");
	}

	private String registrarResultadoValidacion(InputValidator val) {
		String res = null;
		if (!val.isIncorrecta() && !val.isNula())
			res = TILDE_SRC;
		else if (val.isIncorrecta())
			res = ADVERTENCIA_SRC;
		else if (val.isNula() && val.isObligatoria())
			res = ASTERISCO_SRC;
		else if (val.isNula() && !val.isObligatoria())
			res = null;
		return res;
	}

	@DependsOn({ "cantidadValidada", "subintervalosValidada", "semillaValidada", "constanteMultiplicativaValidada",
			"constanteAditivaValidada", "moduloValidada" })
	public boolean isValidacionOk() {
		return (cantidadValidada
				|| (!getIngresarCantidadValidator().isObligatoria() && getIngresarCantidadIconSrc() == null))
				&& (subintervalosValidada || (!getIngresarSubintervalosValidator().isObligatoria()
						&& getIngresarSubintervalosIconSrc() == null))
				&& (semillaValidada
						|| (!getIngresarSemillaValidator().isObligatoria() && getIngresarSemillaIconSrc() == null))
				&& (constanteMultiplicativaValidada || (!getIngresarConstanteMultiplicativaValidator().isObligatoria()
						&& getIngresarConstanteMultiplicativaIconSrc() == null))
				&& (constanteAditivaValidada || (!getIngresarConstanteAditivaValidator().isObligatoria()
						&& getIngresarConstanteAditivaIconSrc() == null))
				&& (moduloValidada || (!getIngresarModuloValidator().isObligatoria()
						&& getIngresarModuloIconSrc() == null));
	}

	public InputValidator getIngresarCantidadValidator() {
		return ingresarCantidadValidator;
	}

	public InputValidator getIngresarSubintervalosValidator() {
		return ingresarSubintervalosValidator;
	}

	public InputValidator getIngresarSemillaValidator() {
		return ingresarSemillaValidator;
	}

	public InputValidator getIngresarConstanteMultiplicativaValidator() {
		return ingresarConstanteMultiplicativaValidator;
	}

	public InputValidator getIngresarConstanteAditivaValidator() {
		return ingresarConstanteAditivaValidator;
	}

	public InputValidator getIngresarModuloValidator() {
		return ingresarModuloValidator;
	}

	@DependsOn("cantidadValidada")
	public String getDefaultTooltiptextIngresarCantidad() {
		String res = null;
		if (getIngresarCantidadValidator().isObligatoria() && !cantidadValidada)
			res = "Este campo es obligatorio.";
		else if (cantidadValidada)
			res = "Cantidad válida.";
		return res;
	}

	@DependsOn("subintervalosValidada")
	public String getDefaultTooltiptextIngresarSubintervalos() {
		String res = null;
		if (getIngresarSubintervalosValidator().isObligatoria() && !subintervalosValidada)
			res = "Este campo es obligatorio.";
		else if (subintervalosValidada)
			res = "Constante multiplicativa válida.";
		return res;
	}

	@DependsOn("semillaValidada")
	public String getDefaultTooltiptextIngresarSemilla() {
		String res = null;
		if (getIngresarSemillaValidator().isObligatoria() && !semillaValidada)
			res = "Este campo es obligatorio.";
		else if (semillaValidada)
			res = "Semilla válida.";
		return res;
	}

	@DependsOn("constanteMultiplicativaValidada")
	public String getDefaultTooltiptextIngresarConstanteMultiplicativa() {
		String res = null;
		if (getIngresarConstanteMultiplicativaValidator().isObligatoria() && !constanteMultiplicativaValidada)
			res = "Este campo es obligatorio.";
		else if (constanteMultiplicativaValidada)
			res = "Constante multiplicativa válida.";
		return res;
	}

	@DependsOn("constanteAditivaValidada")
	public String getDefaultTooltiptextIngresarConstanteAditiva() {
		String res = null;
		if (getIngresarConstanteAditivaValidator().isObligatoria() && !constanteAditivaValidada)
			res = "Este campo es obligatorio.";
		else if (constanteAditivaValidada)
			res = "Constante aditiva válida.";
		return res;
	}

	@DependsOn("moduloValidada")
	public String getDefaultTooltiptextIngresarModulo() {
		String res = null;
		if (getIngresarModuloValidator().isObligatoria() && !moduloValidada)
			res = "Este campo es obligatorio.";
		else if (moduloValidada)
			res = "Módulo válido.";
		return res;
	}

	public String getIngresarCantidadIconSrc() {
		return ingresarCantidadIconSrc;
	}

	public String getIngresarSubintervalosIconSrc() {
		return ingresarSubintervalosIconSrc;
	}

	public String getIngresarSemillaIconSrc() {
		return ingresarSemillaIconSrc;
	}

	public String getIngresarConstanteMultiplicativaIconSrc() {
		return ingresarConstanteMultiplicativaIconSrc;
	}

	public String getIngresarConstanteAditivaIconSrc() {
		return ingresarConstanteAditivaIconSrc;
	}

	public String getIngresarModuloIconSrc() {
		return ingresarModuloIconSrc;
	}

	public boolean isCantidadValidada() {
		return cantidadValidada;
	}

	public boolean isSubintervalosValidada() {
		return subintervalosValidada;
	}

	public boolean isSemillaValidada() {
		return semillaValidada;
	}

	public boolean isConstanteMultiplicativaValidada() {
		return constanteMultiplicativaValidada;
	}

	public boolean isConstanteAditivaValidada() {
		return constanteAditivaValidada;
	}

	public boolean isModuloValidada() {
		return moduloValidada;
	}

	public void setIngresarCantidadIconSrc(String ingresarCantidadIconSrc) {
		this.ingresarCantidadIconSrc = ingresarCantidadIconSrc;
	}

	public void setIngresarSubintervalosIconSrc(String ingresarSubintervalosIconSrc) {
		this.ingresarSubintervalosIconSrc = ingresarSubintervalosIconSrc;
	}

	public void setIngresarSemillaIconSrc(String ingresarSemillaIconSrc) {
		this.ingresarSemillaIconSrc = ingresarSemillaIconSrc;
	}

	public void setIngresarConstanteMultiplicativaIconSrc(String ingresarConstanteMultiplicativaIconSrc) {
		this.ingresarConstanteMultiplicativaIconSrc = ingresarConstanteMultiplicativaIconSrc;
	}

	public void setIngresarConstanteAditivaIconSrc(String ingresarConstanteAditivaIconSrc) {
		this.ingresarConstanteAditivaIconSrc = ingresarConstanteAditivaIconSrc;
	}

	public void setIngresarModuloIconSrc(String ingresarModuloIconSrc) {
		this.ingresarModuloIconSrc = ingresarModuloIconSrc;
	}

	public void setCantidadValidada(boolean cantidadValidada) {
		this.cantidadValidada = cantidadValidada;
	}

	public void setSubintervalosValidada(boolean subintervalosValidada) {
		this.subintervalosValidada = subintervalosValidada;
	}

	public void setSemillaValidada(boolean semillaValidada) {
		this.semillaValidada = semillaValidada;
	}

	public void setConstanteMultiplicativaValidada(boolean constanteMultiplicativaValidada) {
		this.constanteMultiplicativaValidada = constanteMultiplicativaValidada;
	}

	public void setConstanteAditivaValidada(boolean constanteAditivaValidada) {
		this.constanteAditivaValidada = constanteAditivaValidada;
	}

	public void setModuloValidada(boolean moduloValidada) {
		this.moduloValidada = moduloValidada;
	}
	// Fin "Validación"

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getTruncador() {
		formateadorDeNumerosDecimales.setPattern("#.####");
		return formateadorDeNumerosDecimales;
	}

	public String getResultadosSrc() {
		return rB.getPagePathname("resultados");
	}

	public String getInputSrc() {
		return rB.getPagePathname("input");
	}

	public String getGraficaSrc() {
		String src = null;
		if (puntoC != null && puntoC.getHistograma() != null && puntoC.getHistograma().getSerie() != null
				&& puntoC.getHistograma().getSerie().length > 0)
			src = rB.getPagePathname("grafica");
		return src;
	}

	public String getTablaFrecuenciasSrc() {
		String src = null;
		if (puntoC != null && puntoC.getHistograma() != null && puntoC.getHistograma().getSerie() != null
				&& puntoC.getHistograma().getSerie().length > 0)
			src = rB.getPagePathname("tablafrecuencias");
		return src;
	}

	public String getChiCuadradoSrc() {
		String src = null;
		if (puntoC != null && puntoC.getHistograma() != null && puntoC.getHistograma().getSerie() != null
				&& puntoC.getHistograma().getSerie().length > 0)
			src = rB.getPagePathname("chicuadrado.");
		return src;
	}

	public String getChiCuadradoTablaSrc() {
		String src = null;
		if (puntoC != null && puntoC.getHistograma() != null && puntoC.getHistograma().getSerie() != null
				&& puntoC.getHistograma().getSerie().length > 0)
			src = rB.getPagePathname("chicuadrado-tabla");
		return src;
	}

	public void setInputSrc(String inputSrc) {
	}

	public TP1PuntoC getPuntoC() {
		if (puntoC == null)
			puntoC = (TP1PuntoC) SpringUtil.getBean("puntoC");
		return puntoC;
	}

	public void setPuntoC(TP1PuntoC puntoC) {
		this.puntoC = puntoC;
	}

	@DependsOn("puntoC")
	public ListModelList<Double> getLista() {
		lista.clear();
		List<Double> numeros = new ArrayList<>();
		if (puntoC != null && puntoC.getSerie() != null) {
			for (Double d : puntoC.getSerie())
				numeros.add(d);
			lista.addAll(numeros);
			lista.addToSelection(puntoC.getSerie()[puntoC.getSerie().length - 1]);
		}
		return lista;
	}

	public boolean isMostrandoSolucion() {
		return mostrandoSolucion;
	}

	public void setMostrandoSolucion(boolean mostrandoSolucion) {
		this.mostrandoSolucion = mostrandoSolucion;
	}
	
	public String getTituloChiCuadrado() {
		return "Prueba de \u03C7\u2072";
	}


}
