package app.tp1.puntoc;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.GeneradorCongruencialLineal;
import app.utils.Hipotesis;
import app.utils.Histograma;
import app.utils.TipoDistribucion;

@Component
public class TP1PuntoCImpl implements TP1PuntoC {

	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoCImpl.class);
	int cantidad, subintervalos;
	@Autowired
	Histograma histograma;
	@Autowired
	private GeneradorCongruencialLineal generadorCongruencialLineal;
	@Autowired
	ApplicationContext context;

	public TP1PuntoCImpl() {
		super();
	}

	@PostConstruct
	public void init() {
		Hipotesis hipotesis = (Hipotesis) context.getBean("hipotesis");
		TipoDistribucion tipoDistribucion = (TipoDistribucion) context.getBean("distribucionUniforme");
		hipotesis.setTipoDistribucion(tipoDistribucion);
		histograma.postularHipotesis(hipotesis);
	}

	@Bean
	@Scope(value = "prototype")
	static public TP1PuntoC puntoC() {
		return new TP1PuntoCImpl();
	}
	
	@Override
	public void solucionar() {
		histograma.generar(generarSerie(), subintervalos, 0.0d, 1.0d);
	}
	
	@Override
	public void reiniciar() {
		histograma.clear();
	}

	private double[] generarSerie() {
		double[] serie = new double[cantidad];
		for (int i = 0; i < cantidad; i++)
			serie[i] = generadorCongruencialLineal.next();
		return serie;
	}

	@Override
	public double[] getSerie() {
		return histograma.getSerie();
	}

	@Override
	public int getCantidad() {
		return cantidad;
	}

	@Override
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public int getSubintervalos() {
		return subintervalos;
	}

	@Override
	public void setSubintervalos(int subintervalos) {
		this.subintervalos = subintervalos;
	}

	@Override
	public Histograma getHistograma() {
		return histograma;
	}

	@Override
	public GeneradorCongruencialLineal getGeneradorCongruencialLineal() {
		return generadorCongruencialLineal;
	}

}
