package app.tp1.puntoc;

import app.Ejercicio;
import app.utils.GeneradorCongruencialLineal;
import app.utils.Histograma;

public interface TP1PuntoC extends Ejercicio {

	void reiniciar();

	double[] getSerie();
	
	Histograma getHistograma();
	
	int getCantidad();

	void setCantidad(int cantidad);
	
	int getSubintervalos();
	
	void setSubintervalos(int subintervalos);
	
	GeneradorCongruencialLineal getGeneradorCongruencialLineal();

}
