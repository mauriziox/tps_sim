package app.tp1.puntob;

import app.Ejercicio;
import app.utils.Histograma;

public interface TP1PuntoB extends Ejercicio {

	void reiniciar();

	double[] getSerie();
	
	Histograma getHistograma();
	
	int getCantidad();

	void setCantidad(int cantidad);
	
	int getSubintervalos();
	
	void setSubintervalos(int subintervalos);

}
