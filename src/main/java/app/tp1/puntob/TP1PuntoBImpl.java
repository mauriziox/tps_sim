package app.tp1.puntob;

import java.util.Random;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Hipotesis;
import app.utils.Histograma;
import app.utils.TipoDistribucion;

@Component
public class TP1PuntoBImpl implements TP1PuntoB {

	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoBImpl.class);
	int cantidad, subintervalos;
	@Autowired
	Histograma histograma;
	@Autowired
	ApplicationContext context;

	public TP1PuntoBImpl() {
		super();
	}
	
	@PostConstruct
	public void init() {
		Hipotesis hipotesis = (Hipotesis) context.getBean("hipotesis");
		TipoDistribucion tipoDistribucion = (TipoDistribucion) context.getBean("distribucionUniforme");
		hipotesis.setTipoDistribucion(tipoDistribucion);
		histograma.postularHipotesis(hipotesis);
	}

	@Bean
	@Scope(value = "prototype")
	static public TP1PuntoB puntoB() {
		return new TP1PuntoBImpl();
	}
	
	@Override
	public void solucionar() {
		histograma.generar(generarSerie(), subintervalos, 0.0d, 1.0d);
	}
	
	@Override
	public void reiniciar() {
		histograma.clear();
	}

	private double[] generarSerie() {
		double[] serie = new double[cantidad];
		Random r = new Random();
		for (int i = 0; i < cantidad; i++)
			serie[i] = r.nextDouble();
		return serie;
	}

	@Override
	public double[] getSerie() {
		return histograma.getSerie();
	}

	@Override
	public int getCantidad() {
		return cantidad;
	}

	@Override
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public int getSubintervalos() {
		return subintervalos;
	}

	@Override
	public void setSubintervalos(int subintervalos) {
		this.subintervalos = subintervalos;
	}

	@Override
	public Histograma getHistograma() {
		return histograma;
	}

}
