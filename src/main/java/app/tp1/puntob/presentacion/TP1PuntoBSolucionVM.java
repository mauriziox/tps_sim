package app.tp1.puntob.presentacion;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeInt;
import app.presentacion.FormateadorDeNumerosDecimales;
import app.presentacion.IngresarCantidadValidator;
import app.presentacion.IngresarSubintervalosValidator;
import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.presentacion.ResourceBundle;
import app.presentacion.ResourceIndex;
import app.presentacion.SidebarTreeItem;
import app.tp1.puntob.TP1PuntoB;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TP1PuntoBSolucionVM implements Observer {

	private static final String TILDE_SRC = "./images/check_mark.png";
	private static final String ADVERTENCIA_SRC = "./images/warning.png";
	private static final String ASTERISCO_SRC = "./images/asterisk_red.png";
	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoBSolucionVM.class);
	@WireVariable
	private ResourceIndex resourceIndex;
	@WireVariable
	private InputValidator ingresarCantidadValidator;
	@WireVariable
	private InputValidator ingresarSubintervalosValidator;
	private ResourceBundle rB;
	private String ingresarCantidadIconSrc, ingresarSubintervalosIconSrc;
	private boolean cantidadValidada, subintervalosValidada;
	@WireVariable
	private TP1PuntoB puntoB;
	ListModelList<Double> lista;
	@WireVariable
	FormateadorDeNumerosDecimales formateadorDeNumerosDecimales;
	@WireVariable
	ConversorDeInt conversorDeInt;
	private boolean mostrandoSolucion;

	@Init
	public void initSetup() {
		rB = resourceIndex.getResourceBundle("lypmvw");
		lista = new ListModelList<>();
		ingresarCantidadValidator.addObserver(this);
		ingresarSubintervalosValidator.addObserver(this);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	private void inicializarValidaciones() {
	}

	private void inicializarIconosValidaciones() {
		setIngresarCantidadIconSrc(ASTERISCO_SRC);
		setIngresarSubintervalosIconSrc(ASTERISCO_SRC);
	}

	@GlobalCommand
	@NotifyChange({ "lista", "cantidadValidada", "subintervalosValidada", "ingresarCantidadIconSrc",
			"ingresarSubintervalosIconSrc" })
	public void showContent(@BindingParam("page") SidebarTreeItem page) {
		if (page == null || page.getUri() == null || !page.getUri().equals(rB.getZul()))
			clear();
	}

	@Command
	@NotifyChange({ "lista", "cantidadValidada", "subintervalosValidada", "ingresarCantidadIconSrc",
			"ingresarSubintervalosIconSrc", "mostrandoSolucion" })
	public void clear() {
		puntoB.reiniciar();
		setCantidadValidada(false);
		setSubintervalosValidada(false);
		setMostrandoSolucion(false);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	@Command
	@NotifyChange({ "puntoB", "graficaSrc", "chiCuadradoSrc", "tablaFrecuenciasSrc", "mostrandoSolucion" })
	public void run() {
		puntoB.solucionar();
		BindUtils.postNotifyChange(null, null, puntoB, "histograma");
		setMostrandoSolucion(true);
	}

	// "Validación"
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof IngresarCantidadValidator)
			updateValidacion((IngresarCantidadValidator) o);
		if (o instanceof IngresarSubintervalosValidator)
			updateValidacion((IngresarSubintervalosValidator) o);
	}

	private void updateValidacion(IngresarCantidadValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarCantidadIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setCantidadValidada(true);
		else
			setCantidadValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarCantidadIconSrc");
		BindUtils.postNotifyChange(null, null, this, "cantidadValidada");
	}

	private void updateValidacion(IngresarSubintervalosValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarSubintervalosIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setSubintervalosValidada(true);
		else
			setSubintervalosValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarSubintervalosIconSrc");
		BindUtils.postNotifyChange(null, null, this, "subintervalosValidada");
	}

	private String registrarResultadoValidacion(InputValidator val) {
		String res = null;
		if (!val.isIncorrecta() && !val.isNula())
			res = TILDE_SRC;
		else if (val.isIncorrecta())
			res = ADVERTENCIA_SRC;
		else if (val.isNula() && val.isObligatoria())
			res = ASTERISCO_SRC;
		else if (val.isNula() && !val.isObligatoria())
			res = null;
		return res;
	}

	@DependsOn({ "cantidadValidada", "subintervalosValidada" })
	public boolean isValidacionOk() {
		return (cantidadValidada
				|| (!getIngresarCantidadValidator().isObligatoria() && getIngresarCantidadIconSrc() == null))
				&& (subintervalosValidada || (!getIngresarSubintervalosValidator().isObligatoria()
						&& getIngresarSubintervalosIconSrc() == null));
	}

	public InputValidator getIngresarCantidadValidator() {
		return ingresarCantidadValidator;
	}

	public InputValidator getIngresarSubintervalosValidator() {
		return ingresarSubintervalosValidator;
	}

	@DependsOn("cantidadValidada")
	public String getDefaultTooltiptextIngresarCantidad() {
		String res = null;
		if (getIngresarCantidadValidator().isObligatoria() && !cantidadValidada)
			res = "Este campo es obligatorio.";
		else if (cantidadValidada)
			res = "Cantidad válida.";
		return res;
	}

	@DependsOn("subintervalosValidada")
	public String getDefaultTooltiptextIngresarSubintervalos() {
		String res = null;
		if (getIngresarSubintervalosValidator().isObligatoria() && !subintervalosValidada)
			res = "Este campo es obligatorio.";
		else if (subintervalosValidada)
			res = "Constante multiplicativa válida.";
		return res;
	}

	public String getIngresarCantidadIconSrc() {
		return ingresarCantidadIconSrc;
	}

	public String getIngresarSubintervalosIconSrc() {
		return ingresarSubintervalosIconSrc;
	}

	public boolean isCantidadValidada() {
		return cantidadValidada;
	}

	public boolean isSubintervalosValidada() {
		return subintervalosValidada;
	}

	public void setIngresarCantidadIconSrc(String ingresarCantidadIconSrc) {
		this.ingresarCantidadIconSrc = ingresarCantidadIconSrc;
	}

	public void setIngresarSubintervalosIconSrc(String ingresarSubintervalosIconSrc) {
		this.ingresarSubintervalosIconSrc = ingresarSubintervalosIconSrc;
	}

	public void setCantidadValidada(boolean cantidadValidada) {
		this.cantidadValidada = cantidadValidada;
	}

	public void setSubintervalosValidada(boolean subintervalosValidada) {
		this.subintervalosValidada = subintervalosValidada;
	}
	// Fin "Validación"

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getTruncador() {
		formateadorDeNumerosDecimales.setPattern("#.####");
		return formateadorDeNumerosDecimales;
	}

	public String getResultadosSrc() {
		return rB.getPagePathname("resultados");
	}

	public String getInputSrc() {
		return rB.getPagePathname("input");
	}

	public String getGraficaSrc() {
		String src = null;
		if (puntoB != null && puntoB.getHistograma() != null && puntoB.getHistograma().getSerie() != null
				&& puntoB.getHistograma().getSerie().length > 0)
			src = rB.getPagePathname("grafica");
		return src;
	}

	public String getTablaFrecuenciasSrc() {
		String src = null;
		if (puntoB != null && puntoB.getHistograma() != null && puntoB.getHistograma().getSerie() != null
				&& puntoB.getHistograma().getSerie().length > 0)
			src = rB.getPagePathname("tablafrecuencias");
		return src;
	}

	public String getChiCuadradoSrc() {
		String src = null;
		if (puntoB != null && puntoB.getHistograma() != null && puntoB.getHistograma().getSerie() != null
				&& puntoB.getHistograma().getSerie().length > 0)
			src = rB.getPagePathname("chicuadrado.");
		return src;
	}

	public String getChiCuadradoTablaSrc() {
		String src = null;
		if (puntoB != null && puntoB.getHistograma() != null && puntoB.getHistograma().getSerie() != null
				&& puntoB.getHistograma().getSerie().length > 0)
			src = rB.getPagePathname("chicuadrado-tabla");
		return src;
	}

	public void setInputSrc(String inputSrc) {
	}

	public TP1PuntoB getPuntoB() {
		if (puntoB == null)
			puntoB = (TP1PuntoB) SpringUtil.getBean("puntoB");
		return puntoB;
	}

	public void setPuntoB(TP1PuntoB puntoB) {
		this.puntoB = puntoB;
	}

	@DependsOn("puntoB")
	public ListModelList<Double> getLista() {
		lista.clear();
		List<Double> numeros = new ArrayList<>();
		if (puntoB != null && puntoB.getSerie() != null) {
			for (Double d : puntoB.getSerie())
				numeros.add(d);
			lista.addAll(numeros);
			lista.addToSelection(puntoB.getSerie()[puntoB.getSerie().length - 1]);
		}
		return lista;
	}

	public boolean isMostrandoSolucion() {
		return mostrandoSolucion;
	}

	public void setMostrandoSolucion(boolean mostrandoSolucion) {
		this.mostrandoSolucion = mostrandoSolucion;
	}
	
	public String getTituloChiCuadrado() {
		return "Prueba de \u03C7\u2072";
	}

}
