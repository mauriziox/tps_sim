package app;

import java.util.List;

public interface Tabla {
	
	List<String> getEncabezados();
	
	List<DetalleTabla> getDetalles();

}
