package app;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class DetalleTablaImpl implements DetalleTabla {

	Map<String, Object> valores;

	public DetalleTablaImpl() {
		super();
		valores = new HashMap<>();
	}

	@Bean
	@Scope("prototype")
	static public DetalleTabla detalleTabla() {
		return new DetalleTablaImpl();
	}

	@Override
	public Map<String, Object> getValores() {
		return valores;
	}

}
