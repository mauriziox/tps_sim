package app;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class TablaImpl implements Tabla {

	List<String> encabezados;
	List<DetalleTabla> detalles;

	public TablaImpl() {
		super();
		encabezados = new ArrayList<>();
		detalles = new ArrayList<>();
	}

	@Bean
	@Scope("prototype")
	static public Tabla tabla() {
		return new TablaImpl();
	}

	@Override
	public List<String> getEncabezados() {
		return encabezados;
	}

	@Override
	public List<DetalleTabla> getDetalles() {
		return detalles;
	}

}
