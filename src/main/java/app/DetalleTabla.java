package app;

import java.util.Map;

public interface DetalleTabla {

	Map<String, Object> getValores();

}
