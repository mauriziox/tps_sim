package app.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class GeneradorCongruencialLinealImpl implements GeneradorCongruencialLineal {

	static final Logger LOG = LoggerFactory.getLogger(GeneradorCongruencialLinealImpl.class);
	private Long x0;
	private Long xi;
	private Long a;
	private Long c;
	private Long m;

	@Bean
	static public GeneradorCongruencialLineal generadorCongruencialLineal() {
		return new GeneradorCongruencialLinealImpl();
	}

	public Long getX0() {
		return x0;
	}

	@Override
	public void setX0(Long x0) {
		this.x0 = x0;
	}

	@Override
	public Long getA() {
		return a;
	}

	@Override
	public void setA(Long a) {
		this.a = a;
	}

	@Override
	public Long getC() {
		return c;
	}

	@Override
	public void setC(Long c) {
		this.c = c;
	}

	@Override
	public Long getM() {
		return m;
	}

	@Override
	public void setM(Long m) {
		this.m = m;
	}

	@Override
	public Double next() {
		Double ri;
		x0 = x0 == null ? 0 : x0;
		xi = xi == null ? x0 : xi;
		xi = (a * xi + c) % m;
		ri = new Double(xi) / new Double(m);
		return ri;
	}

	@Override
	public void reiniciar() {
		xi = null;
	}

}
