package app.utils;

import java.util.Deque;

public interface IntervaloTestChiCuadrado {

	int getFrecuenciaObservada();

	double getFrecuenciaEsperada();

	ExtremoDeIntervalo getInferior();

	ExtremoDeIntervalo getSuperior();

	Deque<IntervaloHistograma> getIntervalosHistograma();

}
