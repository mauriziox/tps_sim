package app.utils;

import java.util.ArrayDeque;
import java.util.Deque;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class IntervaloTestChiCuadradoImpl implements IntervaloTestChiCuadrado {

	Deque<IntervaloHistograma> intervalosHistograma;

	public IntervaloTestChiCuadradoImpl() {
		super();
		intervalosHistograma = new ArrayDeque<>();
	}

	@Bean
	@Scope(value = "prototype")
	public IntervaloTestChiCuadrado intervaloTestChiCuadradado() {
		return new IntervaloTestChiCuadradoImpl();
	}

	@Override
	public int getFrecuenciaObservada() {
		int fo = 0;
		for (IntervaloHistograma i : intervalosHistograma)
			fo += i.getFrecuenciaObservada();
		return fo;
	}

	@Override
	public double getFrecuenciaEsperada() {
		int fe = 0;
		for (IntervaloHistograma i : intervalosHistograma)
			fe += i.getHistograma().getFrecuenciaEsperada(i);
		return fe;
	}

	@Override
	public ExtremoDeIntervalo getInferior() {
		return intervalosHistograma.getFirst().getInferior();
	}

	@Override
	public ExtremoDeIntervalo getSuperior() {
		return intervalosHistograma.getLast().getSuperior();
	}

	@Override
	public Deque<IntervaloHistograma> getIntervalosHistograma() {
		return intervalosHistograma;
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		if (intervalosHistograma.getFirst().getInferior().isAbierto())
			s.append("( ");
		else
			s.append("[ ");
		s.append(String.format("%.4f", intervalosHistograma.getFirst().getInferior().getValor()));
		s.append(" ; ");
		s.append(String.format("%.4f", intervalosHistograma.getLast().getSuperior().getValor()));
		if (intervalosHistograma.getLast().getSuperior().isAbierto())
			s.append(" )");
		else
			s.append("]");
		return s.toString();
	}

}
