package app.utils;

public interface IntervaloHistograma extends Intervalo {
	
	Histograma getHistograma();
	
	void setHistograma(Histograma histograma);

	int getFrecuenciaObservada();

	void setFrecuenciaObservada(int frecuenciaObservada);

	double getFrecuenciaEsperada();
	
	void setFrecuenciaEsperada(double frecuenciaEsperada);

}
