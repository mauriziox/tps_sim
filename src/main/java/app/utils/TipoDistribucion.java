package app.utils;

public interface TipoDistribucion {

	double getFrecuenciaEsperada(IntervaloHistograma intervalo);

}
