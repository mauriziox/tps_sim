package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class DistribucionUniformeImpl implements DistribucionUniforme {

	Histograma histograma;

	public DistribucionUniformeImpl() {
		super();
	}

	@Bean
	@Scope("singleton")
	static public DistribucionUniforme distribucionUniforme() {
		return new DistribucionUniformeImpl();
	}

	@Override
	public double getFrecuenciaEsperada(IntervaloHistograma intervalo) {
		return intervalo.getHistograma().getCantidadNumeros() / new Double(intervalo.getHistograma().getCantidadIntervalos());
	}

}
