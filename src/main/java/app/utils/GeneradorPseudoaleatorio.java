package app.utils;

public interface GeneradorPseudoaleatorio {

	Double next();
	
	void reiniciar();

}
