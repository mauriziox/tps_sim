package app.utils;

public interface GeneradorUniformeTransformadaInversaAB {
	
	Double next(Double rnd);

	void setA(Double a);

	void setB(Double b);
}
