package app.utils;

import java.util.ArrayDeque;
import java.util.Deque;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.DetalleTabla;
import app.Tabla;

@Component
public class HistogramaImpl implements Histograma {

	static final Logger LOG = LoggerFactory.getLogger(HistogramaImpl.class);
	@Autowired
	ApplicationContext context;
	double[] serie;
	IntervaloHistograma[] intervalos;
	double longitud;
	int cantidadIntervalos;
	Deque<Hipotesis> hipotesis;
	@Autowired
	Tabla tabla;

	public HistogramaImpl() {
		super();
		hipotesis = new ArrayDeque<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public Histograma histograma() {
		return new HistogramaImpl();
	}

	@PostConstruct
	public void init() {
		tabla.getEncabezados().add("k");
		tabla.getEncabezados().add("Intervalo");
		tabla.getEncabezados().add("fo");
		tabla.getEncabezados().add("fe");
	}

	@Override
	public void generar(double[] serie, int cantidadIntervalos, double min, double max) {
		this.serie = serie;
		this.cantidadIntervalos = cantidadIntervalos;
		generarIntervalos(cantidadIntervalos, 0.0d, 1.0d);
		determinarFrecuencias();
		generarTabla();
	}

	@Override
	public void clear() {
		serie = null;
		intervalos = null;
		longitud = 0l;
	}

	@Override
	public double[] getSerie() {
		return serie;
	}

	@Override
	public double[] getSerieUniforme() {
		double[] serieUniforme = new double[serie.length];
		for (int i = 0; i < serieUniforme.length; i++)
			if ((i % intervalos.length) < intervalos.length - 1)
				serieUniforme[i] = ((intervalos[i % intervalos.length].getInferior().getValor())
						+ (intervalos[(i % intervalos.length) + 1].getInferior().getValor())) / 2.0d;
			else
				serieUniforme[i] = intervalos[i % intervalos.length].getInferior().getValor();
		return serieUniforme;
	}

	@Override
	public double getLongitud() {
		return longitud;
	}

	@Override
	public IntervaloHistograma[] getIntervalos() {
		return intervalos;
	}

	private void determinarFrecuencias() {
		for (Double d : serie) {
			IntervaloHistograma i = intervalos[new Double(Math.floor(d / longitud)).intValue()];
			i.setFrecuenciaObservada(i.getFrecuenciaObservada() + 1);
		}
	}

	@Override
	public Hipotesis exponerHipotesis() {
		return hipotesis.getFirst();
	}

	@Override
	public void postularHipotesis(Hipotesis hipotesis) {
		if (hipotesis.getTipoDistribucion() == null)
			throw new RuntimeException(
					"Error al postular una hipótesis sin hacer referencia a un tipo de distribución.");
		this.hipotesis.addFirst(hipotesis);
	}

	@Override
	public double getFrecuenciaEsperada(IntervaloHistograma intervalo) {
		return exponerHipotesis() != null ? exponerHipotesis().getTipoDistribucion().getFrecuenciaEsperada(intervalo)
				: null;
	}

	@Override
	public int getCantidadNumeros() {
		return serie.length;
	}

	@Override
	public int getCantidadIntervalos() {
		return cantidadIntervalos;
	}

	@Override
	public Tabla getTabla() {
		return tabla;
	}

	private void generarIntervalos(int cantidad, double min, double max) {
		intervalos = new IntervaloHistograma[cantidad];
		longitud = new Double((max - min) / new Double(cantidad));
		IntervaloHistograma intervalo;
		for (double i = 0; i < cantidad; i++) {
			intervalo = (IntervaloHistograma) context.getBean("intervaloHistograma");
			intervalo.setHistograma(this);
			intervalo.setFrecuenciaObservada(0);
			intervalo.setFrecuenciaEsperada(hipotesis.getLast().getTipoDistribucion().getFrecuenciaEsperada(intervalo));
			intervalo.getInferior().setValor(min + i * longitud);
			intervalo.getInferior().setAbierto(false);
			intervalo.getSuperior().setValor(min + longitud + i * longitud);
			intervalo.getSuperior().setAbierto(true);
			intervalos[(int) i] = intervalo;
		}
	}
	
	private void generarTabla() {
		for (int i = 0; i < intervalos.length; i++) {
			DetalleTabla d = (DetalleTabla) context.getBean("detalleTabla");
			d.getValores().put("k", i + 1);
			d.getValores().put("Intervalo", intervalos[i].toString());
			int fo = intervalos[i].getFrecuenciaObservada();
			d.getValores().put("fo", fo);
			double fe = hipotesis.getFirst().getTipoDistribucion().getFrecuenciaEsperada(intervalos[i]);
			d.getValores().put("fe", fe);
			tabla.getDetalles().add(d);
		}
	}

}
