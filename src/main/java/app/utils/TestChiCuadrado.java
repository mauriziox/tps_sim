package app.utils;

import app.Tabla;

public interface TestChiCuadrado {
	
	double calcular(Histograma histograma);
	
	Tabla getTabla();

}
