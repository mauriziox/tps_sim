package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class GeneradorUniformeTransformadaInversaABImpl implements GeneradorUniformeTransformadaInversaAB {
	
	Double a,b;
	
	@Override
	public Double next(Double rnd) {
		Double n = null;
		n = rnd * (b - a);
		return n;
	}
	
	@Bean
	static public GeneradorUniformeTransformadaInversaAB generadorUniformeTransformadaInversaAB() {
		return new GeneradorUniformeTransformadaInversaABImpl();
	}

	public void setA(Double a) {
		this.a = a;
	}

	public void setB(Double b) {
		this.b = b;
	}

}
