package app.utils;

public interface Intervalo {

	ExtremoDeIntervalo getInferior();

	void setInferior(ExtremoDeIntervalo inferior);

	ExtremoDeIntervalo getSuperior();

	void setSuperior(ExtremoDeIntervalo superior);

}
