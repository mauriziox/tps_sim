package app.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class IntervaloHistogramaImpl implements IntervaloHistograma {

	Histograma histograma;
	int frecuenciaObservada;
	double frecuenciaEsperada;
	@Autowired
	ExtremoDeIntervalo inferior, superior;

	public IntervaloHistogramaImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	public IntervaloHistograma intervaloHistograma() {
		return new IntervaloHistogramaImpl();
	}

	@Override
	public int getFrecuenciaObservada() {
		return frecuenciaObservada;
	}

	@Override
	public void setFrecuenciaObservada(int frecuencia) {
		this.frecuenciaObservada = frecuencia;
	}

	@Override
	public ExtremoDeIntervalo getInferior() {
		return inferior;
	}

	@Override
	public void setInferior(ExtremoDeIntervalo inferior) {
		this.inferior = inferior;
	}

	@Override
	public ExtremoDeIntervalo getSuperior() {
		return superior;
	}

	@Override
	public void setSuperior(ExtremoDeIntervalo superior) {
		this.superior = superior;
	}

	@Override
	public Histograma getHistograma() {
		return histograma;
	}

	@Override
	public void setHistograma(Histograma histograma) {
		this.histograma = histograma;
	}

	@Override
	public double getFrecuenciaEsperada() {
		return frecuenciaEsperada;
	}

	@Override
	public void setFrecuenciaEsperada(double frecuenciaEsperada) {
		this.frecuenciaEsperada = frecuenciaEsperada;
	}

	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		if (getInferior().isAbierto())
			s.append("( ");
		else
			s.append("[ ");
		s.append(String.format("%.4f", getInferior().getValor()));
		s.append(" ; ");
		s.append(String.format("%.4f", getSuperior().getValor()));
		if (getSuperior().isAbierto())
			s.append(" )");
		else
			s.append("]");
		return s.toString();
	}

}
