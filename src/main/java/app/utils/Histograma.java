package app.utils;

import app.Tabla;

public interface Histograma {

	void generar(double[] serie, int subintervalos, double min, double max);
	
	void clear();

	double[] getSerie();
	
	double[] getSerieUniforme();
	
	double getLongitud();

	IntervaloHistograma[] getIntervalos();
	
	Hipotesis exponerHipotesis();
	
	void postularHipotesis(Hipotesis hipotesis);
	
	double getFrecuenciaEsperada(IntervaloHistograma intervalo);
	
	int getCantidadNumeros();

	int getCantidadIntervalos();
	
	Tabla getTabla();

}
