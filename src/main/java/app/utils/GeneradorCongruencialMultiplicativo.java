package app.utils;

public interface GeneradorCongruencialMultiplicativo extends GeneradorPseudoaleatorio {

	void setX0(Long x0);

	Long getA();

	void setA(Long a);

	Long getM();

	void setM(Long m);
	
}
