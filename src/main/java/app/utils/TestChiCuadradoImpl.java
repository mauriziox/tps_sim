package app.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.DetalleTabla;
import app.Tabla;

@Component
public class TestChiCuadradoImpl implements TestChiCuadrado {

	static final Logger LOG = LoggerFactory.getLogger(TestChiCuadradoImpl.class);
	Histograma histograma;
	@Autowired
	Tabla tabla;
	@Autowired
	ApplicationContext context;

	public TestChiCuadradoImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TestChiCuadrado testChiCuadrado() {
		return new TestChiCuadradoImpl();
	}

	@PostConstruct
	public void init() {
		tabla.getEncabezados().add("k");
		tabla.getEncabezados().add("Intervalo");
		tabla.getEncabezados().add("fo");
		tabla.getEncabezados().add("fe");
		tabla.getEncabezados().add("fo - fe");
		tabla.getEncabezados().add("(fo - fe)\u2072");
		tabla.getEncabezados().add("(fo - fe)\u2072 / fe");
		tabla.getEncabezados().add("\u2211((fo - fe)\u2072 / fe)");
	}

	@Override
	public double calcular(Histograma histograma) {
		this.histograma = histograma;
		double chiCuadrado = 0.0d;
		List<IntervaloTestChiCuadrado> intervalosTest = construirIntervalos(histograma.getIntervalos());
		for (int j = 0; j < intervalosTest.size(); j++) {
			IntervaloTestChiCuadrado i = intervalosTest.get(j);
			DetalleTabla d = (DetalleTabla) context.getBean("detalleTabla");
			d.getValores().put("k", j + 1);
			d.getValores().put("Intervalo", i.toString());
			int fo = i.getFrecuenciaObservada();
			d.getValores().put("fo", fo);
			double fe = i.getFrecuenciaEsperada();
			d.getValores().put("fe", fe);
			double a = fo - fe; // fo - fe
			d.getValores().put("fo - fe", a);
			double b = Math.pow(a, 2d); // Math.pow((fo - fe), 2d)
			d.getValores().put("(fo - fe)\u2072", b);
			double c = b / fe; // Math.pow((fo - fe), 2d) / fe
			d.getValores().put("(fo - fe)\u2072 / fe", c);
			chiCuadrado += c;
			d.getValores().put("\u2211((fo - fe)\u2072 / fe)", chiCuadrado);
			tabla.getDetalles().add(d);
		}
		return chiCuadrado;
	}

	private List<IntervaloTestChiCuadrado> construirIntervalos(IntervaloHistograma[] intervalosHistograma) {
		List<IntervaloTestChiCuadrado> intervalosTest = new ArrayList<>();
		IntervaloTestChiCuadrado anterior = null;
		for (int i = 0; i < intervalosHistograma.length; i++) {
			IntervaloHistograma[] restantes = null;
			if (i < intervalosHistograma.length - 2)
				restantes = Arrays.copyOfRange(intervalosHistograma, i + 1, intervalosHistograma.length - 1);
			IntervaloTestChiCuadrado aAgregar = null, intervaloTest = anterior == null
					? (IntervaloTestChiCuadrado) context.getBean("intervaloTestChiCuadradado") : anterior;
			if (intervalosHistograma[i].getFrecuenciaObservada() >= 5 && restantes != null
					&& sumatoriaFrecuenciasObservadas(restantes) >= 5) {
				intervaloTest.getIntervalosHistograma().add(intervalosHistograma[i]);
				aAgregar = intervaloTest;
			} else if (intervalosHistograma[i].getFrecuenciaObservada() >= 5 && restantes == null) {
				intervaloTest.getIntervalosHistograma().add(intervalosHistograma[i]);
				aAgregar = intervaloTest;
			} else if (intervalosHistograma[i].getFrecuenciaObservada() >= 5 && restantes != null
					&& sumatoriaFrecuenciasObservadas(restantes) < 5) {
				intervaloTest.getIntervalosHistograma().add(intervalosHistograma[i]);
				for (int j = 0; j < restantes.length; j++)
					intervaloTest.getIntervalosHistograma().add(restantes[j]);
				i += restantes.length - 1;
				aAgregar = intervaloTest;
			} else if (intervaloTest.getFrecuenciaObservada() > 0 && intervaloTest.getFrecuenciaObservada() < 5
					&& intervalosHistograma[i].getFrecuenciaObservada() < 5 && restantes != null
					&& sumatoriaFrecuenciasObservadas(restantes) < 5
					&& intervaloTest.getFrecuenciaObservada() + intervalosHistograma[i].getFrecuenciaObservada()
							+ sumatoriaFrecuenciasObservadas(restantes) < 5) {
				intervalosTest.get(intervalosTest.size() - 1).getIntervalosHistograma()
						.add(intervaloTest.getIntervalosHistograma().getFirst());
				intervalosTest.get(intervalosTest.size() - 1).getIntervalosHistograma().add(intervalosHistograma[i]);
				for (int j = 0; j < restantes.length; j++)
					intervalosTest.get(intervalosTest.size() - 1).getIntervalosHistograma().add(restantes[j]);
				i += restantes.length - 1;
			} else if (intervaloTest.getFrecuenciaObservada() == 0
					&& intervalosHistograma[i].getFrecuenciaObservada() < 5 && restantes != null
					&& sumatoriaFrecuenciasObservadas(restantes) < 5
					&& intervaloTest.getFrecuenciaObservada() + intervalosHistograma[i].getFrecuenciaObservada()
							+ sumatoriaFrecuenciasObservadas(restantes) < 5) {
				intervalosTest.get(intervalosTest.size() - 1).getIntervalosHistograma().add(intervalosHistograma[i]);
				for (int j = 0; j < restantes.length; j++)
					intervalosTest.get(intervalosTest.size() - 1).getIntervalosHistograma().add(restantes[j]);
				i += restantes.length - 1;
			} else if (intervalosHistograma[i].getFrecuenciaObservada() < 5 && i < intervalosHistograma.length - 1) {
				intervaloTest.getIntervalosHistograma().add(intervalosHistograma[i]);
				if (intervaloTest.getFrecuenciaObservada() >= 5)
					aAgregar = intervaloTest;
				else
					anterior = intervaloTest;
			} else if (intervalosHistograma[i].getFrecuenciaObservada() < 5 && i == intervalosHistograma.length - 1) {
				intervalosTest.get(intervalosTest.size() - 1).getIntervalosHistograma().add(intervalosHistograma[i]);
				anterior = null;
			}
			if (aAgregar != null) {
				intervalosTest.add(aAgregar);
				aAgregar = null;
				anterior = null;
			}
		}
		return intervalosTest;
	}

	private int sumatoriaFrecuenciasObservadas(IntervaloHistograma[] intervalosHistograma) {
		int s = 0;
		for (IntervaloHistograma i : intervalosHistograma)
			s += i.getFrecuenciaObservada();
		return s;
	}

	@Override
	public Tabla getTabla() {
		return tabla;
	}

}
