package app.utils;

public interface Hipotesis {
	
	boolean isAceptada();
	
	TipoDistribucion getTipoDistribucion();
	
	void setTipoDistribucion(TipoDistribucion tipo);

}
