package app.utils;

public interface GeneradorCongruencialLineal extends GeneradorPseudoaleatorio {

	void setX0(Long x0);

	Long getA();

	void setA(Long a);

	Long getC();

	void setC(Long c);

	Long getM();

	void setM(Long m);

}
